FROM debian:9.3

RUN apt-get update && \
    apt-get install -y --no-install-recommends git wget ca-certificates openssh-client && \
    git config --global user.email "continuous@evolution.io" && \
    git config --global user.name "Continuous Evolution" && \
    mkdir -p /usr/local/share/ca-certificates/cacert.org && \
    wget -P /usr/local/share/ca-certificates/cacert.org http://www.cacert.org/certs/root.crt http://www.cacert.org/certs/class3.crt && \
    update-ca-certificates && \
    mkdir /root/.ssh/ && \
    touch /root/.ssh/config && \
    echo "Host *\n\tStrictHostKeyChecking no" > /root/.ssh/config

ADD ./build/ /bin

WORKDIR /bin

ENTRYPOINT [ "/bin/cli" ]