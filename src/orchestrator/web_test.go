package orchestrator

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"errors"
	"net/http"
	"net/url"
	"testing"
)

type fakeResponseWriter struct {
	headers     map[string][]string
	res         string
	reponseCode int
}

func (f *fakeResponseWriter) Header() http.Header {
	return f.headers
}
func (f *fakeResponseWriter) Write(b []byte) (int, error) {
	f.res = f.res + string(b)
	return len(b), nil
}

func (f *fakeResponseWriter) WriteHeader(statusCode int) {
	f.reponseCode = statusCode
}

func TestWebHandler(t *testing.T) {
	writer := &fakeResponseWriter{
		headers: make(map[string][]string),
	}
	handler(writer, &http.Request{})
	if writer.headers["Content-Type"][0] != "application/json" {
		t.Fatalf("Wed should only return Content-Type json instead of %s", writer.headers["Content-Type"])
	}
	if len(writer.res) == 0 {
		t.Fatal("Web should always return body")
	}
}

func TestWebHandlerAddProjectNoProject(t *testing.T) {
	writer := &fakeResponseWriter{
		headers: make(map[string][]string),
	}
	url, _ := url.Parse("")
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeDownloadManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloadManager.BuildProjectHook = func(url string) (project.Project, error) {
		return nil, errors.New("fake error")
	}
	handlerProject(fakeSender, fakeDownloadManager)(writer, &http.Request{URL: url})
	if writer.headers["Content-Type"][0] != "application/json" {
		t.Fatalf("Wed should only return Content-Type json instead of %s", writer.headers["Content-Type"])
	}
	if writer.reponseCode != http.StatusBadRequest {
		t.Fatalf("Web should return 400 instead of %d when no query project", writer.reponseCode)
	}
}

func TestWebHandlerAddProjectBadProject(t *testing.T) {
	writer := &fakeResponseWriter{
		headers: make(map[string][]string),
	}
	url, _ := url.Parse("http://127.0.0.1/addProject?project=toto")
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeDownloadManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloadManager.BuildProjectHook = func(url string) (project.Project, error) {
		return nil, errors.New("fake error")
	}
	handlerProject(fakeSender, fakeDownloadManager)(writer, &http.Request{URL: url})
	if writer.headers["Content-Type"][0] != "application/json" {
		t.Fatalf("Wed should only return Content-Type json instead of %s", writer.headers["Content-Type"])
	}
	if writer.reponseCode != http.StatusBadRequest {
		t.Fatalf("Web should return 400 instead of %d when no query project", writer.reponseCode)
	}
}

func TestWebHandlerAddProject(t *testing.T) {
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)

	writer := &fakeResponseWriter{
		headers: make(map[string][]string),
	}
	url, _ := url.Parse("http://127.0.0.1/addProject?project=toto")

	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(o project.Option) {
		if o.Get() != fakeProject {
			t.Fatalf("Bad project receive")
		}
	}

	fakeDownloadManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloadManager.BuildProjectHook = func(string) (project.Project, error) {
		return fakeProject, nil
	}

	handlerProject(fakeSender, fakeDownloadManager)(writer, &http.Request{URL: url})
	if writer.headers["Content-Type"][0] != "application/json" {
		t.Fatalf("Wed should only return Content-Type json instead of %s", writer.headers["Content-Type"])
	}
}
