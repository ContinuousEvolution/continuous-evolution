package orchestrator

//go:generate charlatan -package mocks -output ../mocks/projectAlreadySave.go ProjectAlreadySave

import (
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/project"
	"errors"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

var loggerManager = logrus.WithField("logger", "orchestrator/manager")

//Config is configuration required by orchestrator
type Config struct {
	Web    WebConfig
	Cli    CliConfig
	Github GithubConfig
	Gitlab GitlabConfig
}

//DefaultConfig is the default configuration required by orchestrator
var DefaultConfig = Config{Web: DefaultWebConfig, Cli: DefaultCliConfig, Github: DefaultGithubConfig, Gitlab: DefaultGitlabConfig}

type manager struct {
	sender  Sender
	cliWait *sync.WaitGroup
	config  Config
	workers []WorkerConf
	exiter  func(int)
}

//Manager start a chain, send message to it and close it
type Manager interface {
	Send(project.Option)
	Close()
	Run(downloader.Manager, ProjectAlreadySave) error
}

//ProjectAlreadySave permit to know if a project is already known, useful for github retreiver
type ProjectAlreadySave interface {
	IsEnabled() bool
	Exist(project.Project) bool
}

//NewManager return a Manager configured
func NewManager(config Config, workers []WorkerConf) (Manager, error) {
	var sender Sender
	var err error
	cliWait := &sync.WaitGroup{}
	if config.Web.Enabled || config.Github.Enabled {
		sender, err = NewWorkers(workers, func(p project.Project, err error) {
			loggerManager.WithField("project", p.Fullname()).Info("Project finish")
		})
	} else if config.Cli.Enabled {
		cliWait.Add(1)
		sender, err = NewWorkers(workers, func(p project.Project, err error) {
			defer cliWait.Done()
			if err != nil {
				loggerManager.WithError(err).Error("Error during process")
				os.Exit(1)
			}
		})
	} else {
		return nil, errors.New("You need to specify orchestrator.web=true or orchestrator.cli=true in config file")
	}
	return &manager{
		sender:  sender,
		cliWait: cliWait,
		config:  config,
		workers: workers,
		exiter:  os.Exit,
	}, err
}

func (m *manager) Send(p project.Option) {
	m.sender.Send(p)
}

func (m *manager) Close() {
	//TODO close web and github
	m.sender.Close()
}

func (m *manager) Run(downloaderManager downloader.Manager, projectAlreadySave ProjectAlreadySave) error {
	if m.config.Github.Enabled {
		g, err := newGithub(m.config, m.sender)
		if err != nil {
			return err
		}
		err = g.start(downloaderManager, projectAlreadySave)
		if err != nil {
			return err
		}
	}
	if m.config.Gitlab.Enabled {
		g, err := newGitlab(m.config, m.sender)
		if err != nil {
			return err
		}
		err = g.start(downloaderManager, projectAlreadySave)
		if err != nil {
			return err
		}
	}
	if m.config.Web.Enabled {
		startWeb(m.sender, m.config.Web, downloaderManager)
	} else if m.config.Cli.Enabled {
		m.startCli(m.config.Cli.Path, downloaderManager)
	}
	return nil
}

func (m *manager) startCli(url string, downloaderManager downloader.Manager) {
	defer m.sender.Close()
	loggerCli.Info("Start")
	err := startCli(url, m.sender, downloaderManager)
	if err != nil {
		loggerManager.WithError(err).Error("Can't initialize")
		m.exiter(1)
		return
	}
	m.cliWait.Wait()
}
