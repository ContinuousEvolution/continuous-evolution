package orchestrator

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"errors"
	"testing"
)

func TestNewManager(t *testing.T) {
	_, err := NewManager(Config{Web: WebConfig{}, Cli: CliConfig{}}, []WorkerConf{{Active: true, PoolSize: 1, Callback: func(p project.Option) project.Option {
		return p
	}}})
	if err == nil {
		t.Fatal("No worker enabled must throw error")
	}
	_, err = NewManager(DefaultConfig, []WorkerConf{})
	if err == nil {
		t.Fatal("No worker conf must throw error")
	}
}

func TestRunManagerCli(t *testing.T) {
	projectWorker1 := make(chan project.Project)
	m, err := NewManager(Config{Web: WebConfig{Enabled: false}, Cli: CliConfig{Enabled: true, Path: "test"}}, []WorkerConf{{Active: true, PoolSize: 1, Callback: func(p project.Option) project.Option {
		projectWorker1 <- p.Get()
		return p
	}}})
	if err != nil {
		t.Fatal("Good conf should not return err", err)
	}
	defer m.Close()
	go func() {
		p := <-projectWorker1
		if p.GitURL() != "test" {
			t.Fatal("Should return good project")
		}
	}()
	fakeManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeManager.BuildProjectHook = func(string) (project.Project, error) { return project.New(project.Input{GitURL: "test"}), nil }
	err = m.Run(fakeManager, nil)
	if err != nil {
		t.Fatal("good conf should not throw err", err)
	}
}

func TestRunManagerCliError(t *testing.T) {
	projectWorker1 := make(chan project.Project)
	m, err := NewManager(Config{Web: WebConfig{Enabled: false}, Cli: CliConfig{Enabled: true, Path: "test"}}, []WorkerConf{{Active: true, PoolSize: 1, Callback: func(p project.Option) project.Option {
		projectWorker1 <- p.Get()
		return p
	}}})
	if err != nil {
		t.Fatal("Good conf should not return err", err)
	}
	m.(*manager).exiter = func(code int) {
		if code != 1 {
			t.Fatalf("invalid path in cli mode should exit 1 instead of %d", code)
		}
	}
	defer m.Close()
	go func() {
		<-projectWorker1
		t.Fatal("When project can't be build should not call worker")
	}()

	fakeDownloadManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloadManager.BuildProjectHook = func(url string) (project.Project, error) {
		return nil, errors.New("fake error")
	}
	err = m.Run(fakeDownloadManager, nil)
	if err != nil {
		t.Fatal("Good conf should not throw err", err)
	}
}
