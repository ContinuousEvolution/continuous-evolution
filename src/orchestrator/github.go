package orchestrator

//go:generate charlatan -package mocks -output ../mocks/graphQlQuery.go GraphQlQuery

import (
	"context"
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/project"
	"continuous-evolution/src/project/io"
	"errors"
	"net/http"
	"net/url"
	"time"

	"github.com/shurcooL/githubv4"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

//GithubConfig is configuration required by github to add project based on global user
type GithubConfig struct {
	Enabled       bool
	DurationFetch string
	Instances     []GithubInstance
}

//GithubInstance represent a call to github (e.g. url + token)
type GithubInstance struct {
	APIURL       string
	Login        string
	PrivateToken string
}

//DefaultGithubConfig is the default configuration required by github
var DefaultGithubConfig = GithubConfig{Enabled: false, DurationFetch: "1h", Instances: []GithubInstance{{APIURL: "https://api.github.com", Login: "", PrivateToken: ""}}}

var loggerGithub = logrus.WithField("logger", "orchestrator/github")

//GraphQlQuery permit to do graphql query with variables
type GraphQlQuery interface {
	Query(ctx context.Context, q interface{}, variables map[string]interface{}) error
}

type github struct {
	config         GithubConfig
	durationFetch  time.Duration
	sender         Sender
	poison         chan bool
	httpFactory    func(login string, token string) project.HTTP
	graphQlFactory func(url string, httpClient *http.Client) GraphQlQuery
}

type ownerRepository struct {
	Login githubv4.String
}

type repository struct {
	URL   *githubv4.URI
	Name  githubv4.String
	Owner ownerRepository
}

type gqlGithubQuery struct {
	Viewer struct {
		Login        githubv4.String
		CreatedAt    githubv4.DateTime
		Repositories struct {
			Nodes    []repository
			PageInfo struct {
				EndCursor   githubv4.String
				HasNextPage githubv4.Boolean
			}
			TotalCount githubv4.Int
		} `graphql:"repositories(first: 100, after: $repositoriesCursor, affiliations:[OWNER,COLLABORATOR,ORGANIZATION_MEMBER])"`
	}
}

func newGithub(config Config, sender Sender) (*github, error) {
	d, err := time.ParseDuration(config.Github.DurationFetch)
	if err != nil {
		return nil, err
	}
	for _, instance := range config.Github.Instances {
		if instance.Login == "" || instance.PrivateToken == "" {
			return nil, errors.New("the instance " + instance.APIURL + " must have login and token")
		}
	}
	return &github{
		config:        config.Github,
		durationFetch: d,
		sender:        sender,
		poison:        make(chan bool),
		httpFactory: func(login string, token string) project.HTTP {
			return io.NewHTTP(login, token)
		},
		graphQlFactory: func(url string, httpClient *http.Client) GraphQlQuery {
			return githubv4.NewEnterpriseClient(url, httpClient)
		},
	}, nil
}

//start launch in goroutine every $config.DurationFetch a fetch to get data
func (g *github) start(downloaderManager downloader.Manager, projectAlreadySave ProjectAlreadySave) error {
	if !projectAlreadySave.IsEnabled() {
		return errors.New("you can't enable orchestrator.gitlab without enable scheduler")
	}
	go func() {
		if err := g.acceptInvitation(); err != nil {
			loggerGithub.WithError(err).Error("error during accept invitation")
		}
		if err := g.fetch(downloaderManager, projectAlreadySave); err != nil {
			loggerGithub.WithError(err).Error("bad fetch")
		}
		for {
			select {
			case <-time.Tick(g.durationFetch):
				if err := g.acceptInvitation(); err != nil {
					loggerGithub.WithError(err).Error("error during accept invitation")
				}
				if err := g.fetch(downloaderManager, projectAlreadySave); err != nil {
					loggerGithub.WithError(err).Error("bad fetch")
				}
			case <-g.poison:
				return
			}
		}
	}()
	loggerWeb.WithField("duration", g.config.DurationFetch).Info("Server get projects from github")
	return nil
}

type githubAPIInvitationRepository struct {
	URL string `json:"url"`
}

type githubAPIInvitationOrganization struct {
	Organization struct {
		Login string `json:"login"`
	} `json:"organization"`
}

type githubActive struct {
	State string `json:"state"`
}

func (g *github) acceptInvitation() error {
	for _, instance := range g.config.Instances {
		httpClient := g.httpFactory(instance.Login, instance.PrivateToken)
		//repository invitation
		invitationsRepo := make([]githubAPIInvitationRepository, 0)
		err := httpClient.Get(instance.APIURL+"/user/repository_invitations", &invitationsRepo)
		if err != nil {
			loggerGithub.WithError(err).Error("Error during fetch of repository invitation")
			return err
		}
		loggerGithub.WithField("nb", len(invitationsRepo)).Info("Invitations repository")
		for _, invitation := range invitationsRepo {
			err := httpClient.Patch(invitation.URL, nil, nil)
			if err != nil {
				loggerGithub.WithField("url", invitation.URL).WithError(err).Error("Can't accept repository invitation, maybe next time we call it!")
			}
		}
		//organization invitation
		invitationsOrga := make([]githubAPIInvitationOrganization, 0)
		err = httpClient.Get(instance.APIURL+"/user/memberships/orgs?state=pending", &invitationsOrga)
		if err != nil {
			loggerGithub.WithError(err).Error("Error during fetch of organization invitation")
			return err
		}
		loggerGithub.WithField("nb", len(invitationsOrga)).Info("Invitations organization")
		for _, invitation := range invitationsOrga {
			url := instance.APIURL + "/user/memberships/orgs/" + invitation.Organization.Login
			err := httpClient.Patch(url, githubActive{State: "active"}, nil)
			if err != nil {
				loggerGithub.WithField("url", url).WithError(err).Error("Can't accept organization invitation, maybe next time we call it!")
			}
		}
	}
	return nil
}

func (g *github) fetch(downloaderManager downloader.Manager, projectAlreadySave ProjectAlreadySave) error {
	for _, instance := range g.config.Instances {
		src := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: instance.PrivateToken},
		)
		httpClient := oauth2.NewClient(context.Background(), src)
		client := g.graphQlFactory(instance.APIURL+"/graphql", httpClient)

		variables := map[string]interface{}{
			"repositoriesCursor": (*githubv4.String)(nil), // Null after argument to get first page.
		}

		q := &gqlGithubQuery{}
		for {
			err := client.Query(context.Background(), q, variables)
			if err != nil {
				return err
			}
			loggerGithub.WithField("nbProjects", len(q.Viewer.Repositories.Nodes)).Info("Github fetch projects")
			for _, p := range q.Viewer.Repositories.Nodes {
				p.URL.User = url.UserPassword(string(q.Viewer.Login), instance.PrivateToken)
				pro, err := downloaderManager.BuildProject(p.URL.String() + ".git")
				if err != nil {
					loggerGithub.WithField("project", p.Owner.Login+"/"+p.Name).WithError(err).Error("skip project because can't be created")
				} else {
					if !projectAlreadySave.Exist(pro) {
						g.sender.Send(project.NewOptional(pro))
					}
				}
			}
			if !q.Viewer.Repositories.PageInfo.HasNextPage {
				break
			}
			variables["repositoriesCursor"] = githubv4.NewString(q.Viewer.Repositories.PageInfo.EndCursor)
		}
	}
	return nil
}

func (g *github) close() {
	g.poison <- true
	close(g.poison)
	g.sender.Close()
}
