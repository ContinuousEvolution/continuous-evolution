package scheduler

import (
	"errors"
	"testing"
)

func TestMetric(t *testing.T) {
	m := newMetric()
	if !m.CreateDate.IsZero() {
		t.Fatal("metric CreateDate should be zero")
	}
	if !m.DownloadDate.IsZero() {
		t.Fatal("metric DownloadDate should be zero")
	}
	if m.DownloadError != "" {
		t.Fatal("metric DownloadError should be nil")
	}
	if !m.PackageFromFileDate.IsZero() {
		t.Fatal("metric PackageFromFileDate should be zero")
	}
	if m.PackageFromFileError != "" {
		t.Fatal("metric PackageFromFileError should be nil")
	}
	if !m.UpdateDate.IsZero() {
		t.Fatal("metric UpdateDate should be zero")
	}
	if m.UpdateError != "" {
		t.Fatal("metric UpdateError should be nil")
	}
	if !m.ReportDate.IsZero() {
		t.Fatal("metric ReportDate should be zero")
	}
	if m.ReportError != "" {
		t.Fatal("metric ReportError should be nil")
	}
	if !m.DeleteDate.IsZero() {
		t.Fatal("metric DeleteDate should be zero")
	}
	if m.DeleteError != "" {
		t.Fatal("metric DeleteError should be nil")
	}
	if m.isStarted() {
		t.Fatal("new metric should not started")
	}
	if m.isFinish() {
		t.Fatal("new metric should not finish")
	}
	if m.hasError() {
		t.Fatal("new metric should not in error")
	}

	m = m.with(CreateState, nil).with(DownloadState, nil).with(PackageFromFileState, nil).with(UpdateState, nil).with(ReportState, nil).with(DeleteState, nil)

	if m.CreateDate.IsZero() {
		t.Fatal("metric CreateDate should be zero")
	}
	if m.DownloadDate.IsZero() {
		t.Fatal("metric DownloadDate should be zero")
	}
	if m.DownloadError != "" {
		t.Fatal("metric DownloadError should be nil")
	}
	if m.PackageFromFileDate.IsZero() {
		t.Fatal("metric PackageFromFileDate should be zero")
	}
	if m.PackageFromFileError != "" {
		t.Fatal("metric PackageFromFileError should be nil")
	}
	if m.UpdateDate.IsZero() {
		t.Fatal("metric UpdateDate should be zero")
	}
	if m.UpdateError != "" {
		t.Fatal("metric UpdateError should be nil")
	}
	if m.ReportDate.IsZero() {
		t.Fatal("metric ReportDate should be zero")
	}
	if m.ReportError != "" {
		t.Fatal("metric ReportError should be nil")
	}
	if m.DeleteDate.IsZero() {
		t.Fatal("metric DeleteDate should be zero")
	}
	if m.DeleteError != "" {
		t.Fatal("metric DeleteError should be nil")
	}
	if !m.isStarted() {
		t.Fatal("metric should be started")
	}
	if !m.isFinish() {
		t.Fatal("metric should be finish")
	}
	if m.hasError() {
		t.Fatal("metric should not have error")
	}

	m = m.with(CreateState, nil).with(DownloadState, nil).with(PackageFromFileState, nil).with(UpdateState, nil).with(ReportState, nil).with(DeleteState, errors.New("fake error"))

	if !m.hasError() {
		t.Fatal("metric should be in error")
	}
}
