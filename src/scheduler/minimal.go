package scheduler

import (
	"continuous-evolution/src/project"
	"time"
)

//minimal is a sub representation of a project used internally by scheduler to save only properties required
type minimalProject struct {
	GitURL       string     `json:"gitUrl"`
	Host         string     `json:"host"`
	Organisation string     `json:"organisation"`
	Name         string     `json:"name"`
	Login        string     `json:"login"`
	Token        string     `json:"token"`
	DeletedDate  *time.Time `json:"DeletedDate"`
	Metrics      []metric   `json:"metrics"`
}

func newMinimal(p project.Project) minimalProject {
	return minimalProject{Metrics: []metric{newMetric().withCreateDate()}}.copy(p)
}

func (m minimalProject) copy(p project.Project) minimalProject {
	return minimalProject{
		GitURL:       p.GitURL(),
		Host:         p.Host(),
		Organisation: p.Organisation(),
		Name:         p.Name(),
		Login:        p.Login(),
		Token:        p.Token(),
		DeletedDate:  m.DeletedDate,
		Metrics:      m.Metrics,
	}
}

func (m minimalProject) delete() minimalProject {
	now := time.Now()
	return minimalProject{
		GitURL:       m.GitURL,
		Host:         m.Host,
		Organisation: m.Organisation,
		Name:         m.Name,
		Login:        m.Login,
		Token:        m.Token,
		DeletedDate:  &now,
		Metrics:      m.Metrics,
	}
}

func (m minimalProject) hasError() bool {
	last := m.Metrics[len(m.Metrics)-1]
	return last.hasError()
}

func (m minimalProject) isWorking() bool {
	last := m.Metrics[len(m.Metrics)-1]
	return last.isStarted() && !last.isFinish()
}

func (m minimalProject) lastUpdateDate() time.Time {
	last := m.Metrics[len(m.Metrics)-1]
	return last.DeleteDate
}
