package scheduler

import "time"

// State represent the current state of a project
// Normal cycle is create -> download -> packageFromFile -> update -> report -> delete
type State string

const (
	// CreateState when the project come in the system
	CreateState State = "create"
	// DownloadState when the project is downloaded (git today)
	DownloadState State = "download"
	// PackageFromFileState found packages to updates and updaters corresponding
	PackageFromFileState State = "packageFromFile"
	// UpdateState realize the update with an updater
	UpdateState State = "update"
	// ReportState push updated code and make a (pull | merge)-request
	ReportState State = "report"
	// DeleteState delete local files of the project
	DeleteState State = "delete"
)

type metric struct {
	CreateDate           time.Time
	DownloadDate         time.Time
	DownloadError        string
	PackageFromFileDate  time.Time
	PackageFromFileError string
	UpdateDate           time.Time
	UpdateError          string
	ReportDate           time.Time
	ReportError          string
	DeleteDate           time.Time
	DeleteError          string
}

func newMetric() metric {
	return metric{}
}

func (m metric) isStarted() bool {
	return !m.CreateDate.IsZero()
}

func (m metric) isFinish() bool {
	return !m.CreateDate.IsZero() &&
		!m.DownloadDate.IsZero() &&
		!m.PackageFromFileDate.IsZero() &&
		!m.UpdateDate.IsZero() &&
		!m.ReportDate.IsZero() &&
		!m.DeleteDate.IsZero()
}

func (m metric) hasError() bool {
	return m.DownloadError != "" ||
		m.PackageFromFileError != "" ||
		m.UpdateError != "" ||
		m.ReportError != "" ||
		m.DeleteError != ""
}

func (m metric) with(state State, err error) metric {
	if state == CreateState {
		return m.withCreateDate()
	} else if state == DownloadState {
		return m.withDownloadDate(err)
	} else if state == PackageFromFileState {
		return m.withPackageFromFileDate(err)
	} else if state == UpdateState {
		return m.withUpdateDate(err)
	} else if state == ReportState {
		return m.withReportDate(err)
	} else {
		return m.withDeleteDate(err)
	}
}

func errorOrEmpty(err error) string {
	if err == nil {
		return ""
	}
	return err.Error()
}

func (m metric) withCreateDate() metric {
	return metric{
		CreateDate:           time.Now(),
		DownloadDate:         m.DownloadDate,
		DownloadError:        m.DownloadError,
		PackageFromFileDate:  m.PackageFromFileDate,
		PackageFromFileError: m.PackageFromFileError,
		UpdateDate:           m.UpdateDate,
		UpdateError:          m.UpdateError,
		ReportDate:           m.ReportDate,
		ReportError:          m.ReportError,
		DeleteDate:           m.DeleteDate,
		DeleteError:          m.DeleteError,
	}
}

func (m metric) withDownloadDate(err error) metric {
	return metric{
		CreateDate:           m.CreateDate,
		DownloadDate:         time.Now(),
		DownloadError:        errorOrEmpty(err),
		PackageFromFileDate:  m.PackageFromFileDate,
		PackageFromFileError: m.PackageFromFileError,
		UpdateDate:           m.UpdateDate,
		UpdateError:          m.UpdateError,
		ReportDate:           m.ReportDate,
		ReportError:          m.ReportError,
		DeleteDate:           m.DeleteDate,
		DeleteError:          m.DeleteError,
	}
}

func (m metric) withPackageFromFileDate(err error) metric {
	return metric{
		CreateDate:           m.CreateDate,
		DownloadDate:         m.DownloadDate,
		DownloadError:        m.DownloadError,
		PackageFromFileDate:  time.Now(),
		PackageFromFileError: errorOrEmpty(err),
		UpdateDate:           m.UpdateDate,
		UpdateError:          m.UpdateError,
		ReportDate:           m.ReportDate,
		ReportError:          m.ReportError,
		DeleteDate:           m.DeleteDate,
		DeleteError:          m.DeleteError,
	}
}

func (m metric) withUpdateDate(err error) metric {
	return metric{
		CreateDate:           m.CreateDate,
		DownloadDate:         m.DownloadDate,
		DownloadError:        m.DownloadError,
		PackageFromFileDate:  m.PackageFromFileDate,
		PackageFromFileError: m.PackageFromFileError,
		UpdateDate:           time.Now(),
		UpdateError:          errorOrEmpty(err),
		ReportDate:           m.ReportDate,
		ReportError:          m.ReportError,
		DeleteDate:           m.DeleteDate,
		DeleteError:          m.DeleteError,
	}
}

func (m metric) withReportDate(err error) metric {
	return metric{
		CreateDate:           m.CreateDate,
		DownloadDate:         m.DownloadDate,
		DownloadError:        m.DownloadError,
		PackageFromFileDate:  m.PackageFromFileDate,
		PackageFromFileError: m.PackageFromFileError,
		UpdateDate:           m.UpdateDate,
		UpdateError:          m.UpdateError,
		ReportDate:           time.Now(),
		ReportError:          errorOrEmpty(err),
		DeleteDate:           m.DeleteDate,
		DeleteError:          m.DeleteError,
	}
}

func (m metric) withDeleteDate(err error) metric {
	return metric{
		CreateDate:           m.CreateDate,
		DownloadDate:         m.DownloadDate,
		DownloadError:        m.DownloadError,
		PackageFromFileDate:  m.PackageFromFileDate,
		PackageFromFileError: m.PackageFromFileError,
		UpdateDate:           m.UpdateDate,
		UpdateError:          m.UpdateError,
		ReportDate:           m.ReportDate,
		ReportError:          m.ReportError,
		DeleteDate:           time.Now(),
		DeleteError:          errorOrEmpty(err),
	}
}
