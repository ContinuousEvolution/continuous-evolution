package scheduler

import (
	"bufio"
	"context"
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/orchestrator"
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

//Config is configuration required by scheduler
type Config struct {
	Enabled             bool
	PathDb              string
	WaitDurationProcess string
}

//DefaultConfig is the default configuration required by scheduler
var DefaultConfig = Config{Enabled: false, PathDb: "", WaitDurationProcess: "24h"}

var loggerScheduler = logrus.WithField("logger", "scheduler/scheduler")

type scheduler struct {
	config              Config
	waitDurationProcess time.Duration
	ctxStop             context.Context
	cancelStop          context.CancelFunc
	muProjects          sync.Mutex
	projects            []minimalProject
}

//Scheduler is responsible to schedule update of project stored in config file
//Start to start checking which project need update
//Exist
//Save(project.project) to update or insert a project
//Delete
//Close to gracefully kill scheduler
type Scheduler interface {
	IsEnabled() bool
	Start(orchestrator.Sender, downloader.Manager) error
	Exist(project.Project) bool
	Retrieve(host string, organisation string, name string) (string, string, bool)
	Save(state State) func(project.Option) project.Option
	Delete(project.Project) error
	Close()
}

//NewScheduler return a scheduler which is responsible to
// * load projects from config file
// * fire an update every duration depending on project.LastUpdatedDate
//Call Start() to start the scheduler
//Call Delete() to remove project
//Call Close() to stop it gracefully
func NewScheduler(config Config) (Scheduler, error) {
	if !config.Enabled {
		return &scheduler{}, nil
	}
	if config.PathDb == "" {
		return nil, errors.New("You must set a valid pathDb")
	}
	d, err := time.ParseDuration(config.WaitDurationProcess)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(context.Background())

	sc := &scheduler{
		config:              config,
		waitDurationProcess: d,
		ctxStop:             ctx,
		cancelStop:          cancel,
		projects:            make([]minimalProject, 0),
	}

	fileConfig, err := os.Open(sc.config.PathDb)
	if err != nil {
		return nil, err
	}
	defer fileConfig.Close()
	scanner := bufio.NewScanner(fileConfig)
	scanner.Split(bufio.ScanLines)

	sc.muProjects.Lock()
	defer sc.muProjects.Unlock()

	for scanner.Scan() {
		err := json.Unmarshal(scanner.Bytes(), &sc.projects)
		if err != nil {
			loggerScheduler.WithError(err).Error("Can't unmarshal project in config")
			return nil, err
		}
	}
	return sc, nil
}

func (s *scheduler) IsEnabled() bool {
	return s.config.Enabled
}

func (s *scheduler) Start(sender orchestrator.Sender, downloaderManager downloader.Manager) error {
	if !s.config.Enabled {
		return errors.New("You can't call start if scheduler is not enabled")
	}
	go func() {
		for {
			found, nextMinimalProject, nextProjectToUpdateDuration := s.nextProjectToUpdate()
			select {
			case <-time.After(nextProjectToUpdateDuration): //wait even if found is false to not while(1) (if no project in file for example)
				if found {
					nextProject, err := downloaderManager.BuildProject(nextMinimalProject.GitURL)
					if err != nil {
						loggerScheduler.WithField("git url", nextMinimalProject.GitURL).WithError(err).Error("Can't pass from minimalProject to project !!??")
						break
					}
					sender.Send(project.NewOptional(nextProject))
					time.Sleep(1 * time.Second) //give time to worker.Chain Save project to not while(1) on same project
				}
			case <-s.ctxStop.Done():
				return
			}
		}
	}()
	return nil
}

func (s *scheduler) nextProjectToUpdate() (bool, minimalProject, time.Duration) {
	s.muProjects.Lock()
	defer s.muProjects.Unlock()
	nextProjectToUpdateDuration := 1 * time.Minute //if no project present we wait 1 minute
	var nextProject minimalProject
	found := false
	for _, project := range s.projects {
		if project.DeletedDate == nil && !project.isWorking() && !project.hasError() {
			nextProjectDuration := s.waitDurationProcess - time.Since(project.lastUpdateDate())
			if !found || nextProjectDuration < nextProjectToUpdateDuration {
				nextProjectToUpdateDuration = nextProjectDuration
				nextProject = project
				found = true
			}
		}
	}
	//wait 1 second at leat (useful for debugging) maybe externalize configuration this time or pass to x milliseconds
	if nextProjectToUpdateDuration < 1*time.Second {
		nextProjectToUpdateDuration = 1 * time.Second
	}
	return found, nextProject, nextProjectToUpdateDuration
}

func (s *scheduler) Exist(project project.Project) bool {
	for _, savedProject := range s.projects {
		if project.IsSame(savedProject.Host, savedProject.Organisation, savedProject.Name) {
			return true
		}
	}
	return false
}

//Retrieve return login, password, exist of current host/orga/project
func (s *scheduler) Retrieve(host string, organisation string, name string) (string, string, bool) {
	for _, savedProject := range s.projects {
		if host == savedProject.Host && organisation == savedProject.Organisation && name == savedProject.Name {
			return savedProject.Login, savedProject.Token, true
		}
	}
	return "", "", false
}

//Save search for project url in projects, if found update metrics dependings on state else add new project
func (s *scheduler) Save(state State) func(project.Option) project.Option {
	return func(o project.Option) project.Option {
		return o.Exec(func(p project.Project, e error) (project.Project, error) {
			if !s.config.Enabled {
				return p, errors.New("You can't call Save if scheduler is not enabled")
			}

			s.muProjects.Lock()
			defer s.muProjects.Unlock()
			found := false
			for index, savedProject := range s.projects {
				if p.IsSame(savedProject.Host, savedProject.Organisation, savedProject.Name) {
					if savedProject.DeletedDate != nil {
						return nil, errors.New("project is deleted")
					}
					last := len(savedProject.Metrics) - 1
					if last < 0 {
						last = 0
						savedProject.Metrics = []metric{newMetric()}
					} else if savedProject.Metrics[last].isFinish() {
						savedProject.Metrics = append(savedProject.Metrics, newMetric())
						last = last + 1
					}
					savedProject.Metrics[last] = savedProject.Metrics[last].with(state, e)
					s.projects[index] = savedProject.copy(p)
					found = true
					break
				}
			}
			if !found {
				s.projects = append(s.projects, newMinimal(p))
			}
			err := s.saveProjectsToFile()
			return p, err
		})
	}
}

func (s *scheduler) saveProjectsToFile() error {
	if s.config.PathDb == "" {
		return nil
	}
	json, err := json.Marshal(s.projects)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(s.config.PathDb, json, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (s *scheduler) Delete(p project.Project) error {
	if !s.config.Enabled {
		return nil //no error, just do nothing because downloader can delete project when merge-request is closed
	}
	s.muProjects.Lock()
	defer s.muProjects.Unlock()
	newProjects := make([]minimalProject, len(s.projects))
	found := false
	for i, savedProject := range s.projects {
		if p.IsSame(savedProject.Host, savedProject.Organisation, savedProject.Name) {
			found = true
			savedProject = savedProject.delete()
		}
		newProjects[i] = savedProject
	}
	if found { //if not found no need to update (can arrived when a new project contains error)
		s.projects = newProjects
		return s.saveProjectsToFile()
	}
	return nil
}

func (s *scheduler) Close() {
	if !s.config.Enabled {
		return
	}
	s.cancelStop()
}
