package downloader

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"testing"
)

func TestPullRequestAccept(t *testing.T) {
	pr := newPullRequest(DefaultConfig, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t)).(pullRequest)
	if pr.accept("") {
		t.Fatal("pull-request should not accept empty string")
	}
	if pr.accept("https://api.domain.com/repos/ContinuousEvolution/continuous-evolution/pulls/1") {
		t.Fatal("pull-request should not accept other domain than github.com")
	}
	if !pr.accept("https://api.github.com/repos/ContinuousEvolution/continuous-evolution/pulls/1") {
		t.Fatal("pull-request should accept good https git url")
	}
}

func TestPullRequestBuildProject(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	fakeRetriever := mocks.NewFakeRetrieveProjectDefaultFatal(t)
	fakeRetriever.RetrieveHook = func(typehost string, organisation string, project string) (string, string, bool) {
		return "login", "token", organisation == "ContinuousEvolution"
	}
	pr := newPullRequest(conf, fakeRetriever, mocks.NewFakeDeleteProjectDefaultFatal(t)).(pullRequest)
	var inputProject project.Input
	pr.projectFabric = func(input project.Input) project.Project {
		inputProject = input
		p := mocks.NewFakeProjectDefaultFatal(t)
		p.SetReProcessDistantIDHook = func(id string) project.Project {
			if id != "42" {
				t.Fatalf("ReprocessDistantId should be 42 instead of %s", id)
			}
			return p
		}
		p.SetExcludesHook = func(excludes map[string][]string) project.Project {
			return p
		}
		p.HTTPHook = func() project.HTTP {
			h := mocks.NewFakeHTTPDefaultFatal(t)
			h.GetHook = func(url string, toReceive interface{}) error {
				json.Unmarshal([]byte(`{"state":"open" ,"body":""}`), toReceive)
				return nil
			}
			return h
		}
		return p
	}

	//test good project
	gitURL := "https://api.github.com/repos/ContinuousEvolution/continuous-evolution/pulls/42"
	_, err := pr.buildProject(gitURL)
	if err != nil {
		t.Fatal("merge-request should not throw error when good https git url", err)
	}
	if inputProject.Login != "login" {
		t.Fatalf("merge-request should return good login instead of %s", inputProject.Login)
	}
	if inputProject.Token != "token" {
		t.Fatalf("merge-request should return good token instead of %s", inputProject.Token)
	}
	if inputProject.Host != "github.com" {
		t.Fatalf("merge-request should return good host instead of %s", inputProject.Host)
	}
	if inputProject.TypeHost != project.Github {
		t.Fatalf("merge-request should return good typehsot instead of %s", inputProject.TypeHost)
	}
	if inputProject.Organisation != "ContinuousEvolution" {
		t.Fatalf("merge-request should return good organisation instead of %s", inputProject.Organisation)
	}
	if inputProject.Name != "continuous-evolution" {
		t.Fatalf("merge-request should return good name instead of %s", inputProject.Name)
	}
	if inputProject.GitURL != "https://login:token@github.com/ContinuousEvolution/continuous-evolution.git" {
		t.Fatalf("merge-request should return good url instead of %s", inputProject.GitURL)
	}
	if inputProject.PathToWrite != "/pathtowrite" {
		t.Fatalf("merge-request should take path to write from config instead of %s", inputProject.PathToWrite)
	}
	if inputProject.BranchName != "branchName" {
		t.Fatalf("merge-request should take branch name from config instead of %s", inputProject.BranchName)
	}

	//test with project not found
	gitURL = "https://api.github.com/repos/ContinuousEvolution2/continuous-evolution/pulls/42"
	_, err = pr.buildProject(gitURL)
	if err == nil {
		t.Fatal("pull-request wihtout pre-saved project should throw error", err)
	}
}

func TestPullRequestBuildProjectStateClosed(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	fakeRetriever := mocks.NewFakeRetrieveProjectDefaultFatal(t)
	fakeRetriever.RetrieveHook = func(typehost string, organisation string, name string) (string, string, bool) {
		return "login", "token", true
	}
	fakeDeleter := mocks.NewFakeDeleteProjectDefaultFatal(t)
	fakeDeleter.DeleteHook = func(project.Project) error {
		return nil
	}
	pr := newPullRequest(conf, fakeRetriever, fakeDeleter).(pullRequest)
	pr.projectFabric = func(input project.Input) project.Project {
		p := mocks.NewFakeProjectDefaultFatal(t)
		p.SetReProcessDistantIDHook = func(id string) project.Project {
			return p
		}
		p.SetExcludesHook = func(excludes map[string][]string) project.Project {
			return p
		}
		p.HTTPHook = func() project.HTTP {
			h := mocks.NewFakeHTTPDefaultFatal(t)
			h.GetHook = func(url string, toReceive interface{}) error {
				json.Unmarshal([]byte(`{"state":"close" ,"body":""}`), toReceive)
				return nil
			}
			return h
		}
		return p
	}
	gitURL := "https://api.github.com/repos/ContinuousEvolution/continuous-evolution/pulls/42"
	_, err := pr.buildProject(gitURL)
	if err == nil {
		t.Fatal("closed merge-request should throw error to break chain")
	}
	fakeDeleter.AssertDeleteCalledOnce(t)
}

func TestPullRequestDownload(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	pr := newPullRequest(conf, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t)).(pullRequest)
	fakeGit := &mocks.FakeGit{
		CloneHook: func() error {
			return nil
		},
	}

	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.GitHook = func() (ident1 project.Git) {
		return fakeGit
	}

	p, err := pr.download(fakeProject)
	if p == nil {
		t.Fatal("Good project should be return")
	}
	if err != nil {
		t.Fatal("Good project should not throw error")
	}
	fakeProject.AssertGitCalledOnce(t)
	fakeGit.AssertCloneCalledOnce(t)
}

func TestPullRequestDownloadError(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	pr := newPullRequest(conf, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t)).(pullRequest)
	fakeGit := &mocks.FakeGit{
		CloneHook: func() error {
			return errors.New("fake git clone error")
		},
	}

	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.GitHook = func() (ident1 project.Git) {
		return fakeGit
	}

	p, err := pr.download(fakeProject)
	if p == nil {
		t.Fatal("Error when git clone should not return nil project")
	}
	if err == nil {
		t.Fatal("Git downloader should return error from git")
	}
	fakeProject.AssertGitCalledOnce(t)
	fakeGit.AssertCloneCalledOnce(t)
}
