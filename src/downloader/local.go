package downloader

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/project/io"
	"errors"
	"strings"

	"github.com/sirupsen/logrus"
)

//LocalConfig is configuration required by local downloader
type LocalConfig struct {
	Enabled bool
}

//DefaultLocalConfig is the default configuration required by local downloader
var DefaultLocalConfig = LocalConfig{Enabled: true}

var loggerLocal = logrus.WithField("logger", "downloader/local")

type local struct{ config Config }

func newLocal(config Config) downloader {
	return local{config}
}

func (l local) accept(url string) bool {
	isSlashPrefixed := strings.HasPrefix(url, "/")
	if !isSlashPrefixed {
		return false
	}
	isSlashSuffixed := strings.HasSuffix(url, "/")
	if !isSlashSuffixed {
		return false
	}
	pathExist := io.PathExists(url)
	if !pathExist {
		return false
	}
	isGitPath := io.PathExists(url + "/.git")
	if !isGitPath {
		return false
	}
	loggerLocal.
		WithField("isSlashPrefixed", isSlashPrefixed).
		WithField("isSlashSuffixed", isSlashSuffixed).
		WithField("pathExist", pathExist).
		WithField("isGitPath", isGitPath).
		Debug("Local downloader")
	return true
}

func (l local) buildProject(url string) (project.Project, error) {
	path := strings.Split(url, "/")
	name := path[len(path)-2]
	rest := strings.Join(path[:len(path)-2], "/")

	loggerLocal.WithFields(logrus.Fields{
		"path":        url,
		"PathToWrite": rest,
		"projectName": name,
	}).Info("New project")

	return project.New(project.Input{
		Name:         name,
		GitURL:       url,
		BranchName:   l.config.BranchName,
		PathToWrite:  rest,
		Login:        "",
		Token:        "",
		TypeHost:     project.Local,
		Organisation: "",
	}), nil
}

func (l local) download(project project.Project) (project.Project, error) {
	//no need to download only check if git state is clean
	if project.Git().Diff() {
		return project, errors.New("git working dir is not clean")
	}
	return project, nil
}
