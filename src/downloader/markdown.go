package downloader

import "strings"

func parseBody(bodyDetails string) map[string][]string {
	bodyLines := strings.Split(bodyDetails, "\n")
	lastFile := ""
	pkgToExclude := make(map[string][]string)
	for _, bodyLine := range bodyLines {
		if strings.HasPrefix(bodyLine, "* [") {
			lastFile = strings.Split(bodyLine, "]")[1]
			lastFile = strings.Split(lastFile, ":")[0]
			lastFile = strings.TrimSpace(lastFile)
			if strings.HasPrefix(bodyLine, "* [ ] ") {
				pkgToExclude[lastFile] = make([]string, 0)
			}
		} else if strings.HasPrefix(bodyLine, "    * [ ]") {
			if res, ok := pkgToExclude[lastFile]; ok && len(res) == 0 {
				continue // if file is exclude no need to parse depth
			}
			if _, ok := pkgToExclude[lastFile]; !ok {
				pkgToExclude[lastFile] = make([]string, 0)
			}
			pkgNameCleaned := strings.Split(bodyLine, "]")[1]
			pkgNameCleaned = strings.Split(pkgNameCleaned, ":")[0]
			pkgNameCleaned = strings.TrimSpace(pkgNameCleaned)
			pkgToExclude[lastFile] = append(pkgToExclude[lastFile], pkgNameCleaned)
		}

	}

	return pkgToExclude
}
