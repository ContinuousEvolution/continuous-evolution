package downloader

import (
	"continuous-evolution/src/project"
	"regexp"

	"github.com/sirupsen/logrus"
)

//GitConfig is configuration required by git downloader
type GitConfig struct {
	Enabled   bool
	GitlabURL []string
	GithubURL []string
}

//DefaultGitConfig is the default configuration required by git downloader
var DefaultGitConfig = GitConfig{Enabled: true, GitlabURL: []string{}, GithubURL: []string{}}

var loggerGit = logrus.WithField("logger", "downloader/git")
var regexpProject = regexp.MustCompile(`https://([^:]+):([^@]+)@([^/]+)/([^/]+)/([^\.]+)\.git`)

type git struct {
	config Config
}

func newGit(config Config) downloader {
	return git{config}
}

func (g git) accept(url string) bool {
	res := regexpProject.FindStringSubmatch(url)
	if len(res) == 6 {
		acceptHost, _ := g.acceptHost(res[3])
		return acceptHost
	}
	return false
}

func (g git) acceptHost(host string) (bool, project.TypeHost) {
	for _, h := range g.config.Git.GitlabURL {
		if h == host {
			return true, project.Gitlab
		}
	}
	for _, h := range g.config.Git.GithubURL {
		if h == host {
			return true, project.Github
		}
	}
	return false, project.Unknown
}

func (g git) buildProject(gitURL string) (project.Project, error) {
	result := regexpProject.FindStringSubmatch(gitURL)

	login := result[1]
	token := result[2]
	host := result[3]
	_, typeHost := g.acceptHost(host)
	organisation := result[4]
	projectName := result[5]

	p := project.New(project.Input{
		Name:         projectName,
		GitURL:       gitURL,
		BranchName:   g.config.BranchName,
		PathToWrite:  g.config.PathToWrite,
		Login:        login,
		Token:        token,
		Host:         host,
		TypeHost:     typeHost,
		Organisation: organisation,
	})

	loggerGit.WithField("project", p.Fullname()).Info("New project")

	return p, nil
}

func (g git) download(project project.Project) (project.Project, error) {
	if err := project.Git().Clone(); err != nil {
		loggerGit.Error(err)
		return project, err
	}

	return project, nil
}
