package project

import (
	"errors"
	"testing"
)

func TestOption(t *testing.T) {
	p := &project{}
	o := NewOptional(p)
	if o.IsError() {
		t.Fatal("Option without err should not return true when call IsError()")
	}
	if o.Err() != nil {
		t.Fatal("err shoudl be nil if o.IsError() == false")
	}
	if p != o.Get() || !p.IsSame(string(o.Get().TypeHost()), o.Get().Organisation(), o.Get().Name()) {
		t.Fatal("option should retain a project")
	}
	o2 := o.WithError(errors.New("fake"))
	if o.IsError() {
		t.Fatal("original option should be unaffected by withError")
	}

	if !o2.IsError() {
		t.Fatal("option withErr should return true when call IsError()")
	}
	if o2.Err().Error() != "fake" {
		t.Fatal("option should return good error when calling err()")
	}
}

func TestExecOption(t *testing.T) {
	p := &project{}
	o := NewOptional(p).WithError(errors.New("fake error"))
	o2 := o.Exec(func(p2 Project, e error) (Project, error) {
		if p2 != p {
			t.Fatal("project inside option exec should be the same has external")
		}
		if e.Error() != "fake error" {
			t.Fatal("error inside exec option shoudl be the same has external")
		}
		return &project{}, nil
	})
	if o2.Get() == p {
		t.Fatal("Exec callback should return new option with project from callback")
	}
}

func TestExecIfNoError(t *testing.T) {
	p := &project{}
	o := NewOptional(p)
	o2 := o.ExecIfNoError(func(p2 Project) (Project, error) {
		if p2 != p {
			t.Fatal("project inside option exec should be the same has external")
		}
		return &project{}, nil
	})
	if o2.Get() == p {
		t.Fatal("ExecIfNoError callback should return new option with project from callback")
	}

	p = &project{}
	o = NewOptional(p).WithError(errors.New("fake error"))
	called := false
	o2 = o.ExecIfNoError(func(p2 Project) (Project, error) {
		called = true
		return &project{}, nil
	})
	if called {
		t.Fatal("ExecIfNoError should not call callback if has error")
	}
	if o2 != o {
		t.Fatal("ExecIfNoError should return current option if has error")
	}
}
