package deleter

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"os"
	"testing"
)

func TestDeleter(t *testing.T) {
	err := os.MkdirAll("/tmp/deleter/test", os.ModePerm)
	if err != nil {
		t.Fatal("Can't create /tmp/deleter/test dir !?")
	}
	err = os.MkdirAll("/tmp/deleter/test2", os.ModePerm)
	if err != nil {
		t.Fatal("Can't create /tmp/deleter/test2 dir !?")
	}
	p := mocks.NewFakeProjectDefaultFatal(t)
	p.AbsoluteDirectoyHook = func() string {
		return "/tmp/deleter/test"
	}
	o := Delete(project.NewOptional(p))
	if o.IsError() {
		t.Fatal("Valid project should not throw err", o.Err())
	}
	_, err = os.Stat("/tmp/deleter/test")
	if !os.IsNotExist(err) {
		t.Fatal("Delete should delete directory /tmp/deleter/test")
	}
	_, err = os.Stat("/tmp/deleter/test2")
	if os.IsNotExist(err) {
		t.Fatal("Delete should not delete directory /tmp/deleter/test2")
	}
	err = os.RemoveAll("/tmp/deleter/")
	if err != nil {
		t.Fatal("Can't remove /tmp/deleter/ !?", err)
	}
}

func TestDeleterWithEmptyProject(t *testing.T) {
	o := Delete(project.NewOptional(project.New(project.Input{})))
	if !o.IsError() {
		t.Fatal("Empty project should throw error")
	}
}
