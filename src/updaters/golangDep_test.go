package updaters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"testing"
)

func TestGolangDepIsPackageFile(t *testing.T) {
	res := golangDepIsPackageFile("Gopkg.toml", fakeFileInfo{name: "Gopkg.toml", isDir: false})
	if !res {
		t.Fatal("golangDep updater should return true for Gopkg.toml")
	}
	res = golangDepIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("golangDep updater should return false for other")
	}
	res = golangDepIsPackageFile("Gopkg.toml", fakeFileInfo{name: "Gopkg.toml", isDir: true})
	if res {
		t.Fatal("golangDep updater should return false for directory even if its name is Gopkg.toml")
	}
}

type dataTestGolangDep struct {
	name           string
	statusData     string
	exclude        []string
	nbUpdatedDep   int
	callRealUpdate bool
	mustBeValid    bool
}

var dataGolangDep = []dataTestGolangDep{
	{name: "bad json status", statusData: "{", exclude: []string{}, nbUpdatedDep: 0, callRealUpdate: false, mustBeValid: false},
	{name: "empty json status", statusData: "[]", exclude: []string{}, nbUpdatedDep: 0, callRealUpdate: false, mustBeValid: true},
	{name: "one dep no update", statusData: `[{"ProjectRoot":"logrus","Revision":"1","Latest":"1"}]`, exclude: []string{}, nbUpdatedDep: 0, callRealUpdate: false, mustBeValid: true},
	{name: "one dep", statusData: `[{"ProjectRoot":"logrus","Revision":"1","Latest":"2"}]`, exclude: []string{}, nbUpdatedDep: 1, callRealUpdate: true, mustBeValid: true},
	{name: "two dep with one no update", statusData: `[{"ProjectRoot":"logrus","Revision":"1","Latest":"2"},{"ProjectRoot":"logrus2","Revision":"1","Latest":"1"}]`, exclude: []string{}, nbUpdatedDep: 1, callRealUpdate: true, mustBeValid: true},
	//if i have one dep and i exclude it i should not have updated dep
	{name: "exclude", statusData: `[{"ProjectRoot":"logrus","Revision":"1","Latest":"2"}]`, exclude: []string{"logrus"}, nbUpdatedDep: 1, callRealUpdate: true, mustBeValid: true},
	{name: "exclude one", statusData: `[{"ProjectRoot":"logrus","Revision":"1","Latest":"2"},{"ProjectRoot":"logrus2","Revision":"1","Latest":"2"}]`, exclude: []string{"logrus"}, nbUpdatedDep: 2, callRealUpdate: true, mustBeValid: true},
}

func TestDepUpdate(t *testing.T) {
	d := newGolangDep(DefaultConfig)
	dep := d.(*golangDepUpdater)
	for _, test := range dataGolangDep {
		t.Run(test.name, func(t *testing.T) {
			fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
			fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
				if len(cmd) == 2 && cmd[0] == "status" && cmd[1] == "-json" {
					return []byte(test.statusData)
				} else if !test.callRealUpdate {
					t.Fatal("Docker has been called with bad args")
				}
				return []byte("")
			}
			dep.dockerClient = fakeDocker
			dependencies, err := dep.Update("path", project.Package{Path: "pkg", Type: project.PackageType("golangDep"), UpdatedDependencies: []project.Dependency{}}, test.exclude)
			if test.mustBeValid && err != nil {
				t.Fatal("Valid test should not throw err", err)
			}
			if !test.mustBeValid && err == nil {
				t.Fatal("Invalid test should throw err")
			}
			if len(dependencies) != test.nbUpdatedDep {
				t.Fatalf("Nb dependencies updated should be %d instead of %d", test.nbUpdatedDep, len(dependencies))
			}
		})
	}
}
