package updaters

import (
	"bytes"
	"continuous-evolution/src/project"
	"io/ioutil"
	"os"
	"regexp"

	"github.com/sirupsen/logrus"
)

//DockerComposeConfig is configuration required by docker-compose updater
type DockerComposeConfig struct {
	Enabled bool
}

//DefaultDockerComposeConfig is the default configuration required by docker-compose updater
var DefaultDockerComposeConfig = DockerComposeConfig{Enabled: true}

var loggerDockerCompose = logrus.WithField("logger", "updaters/dockerCompose")

func dockerComposeIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "docker-compose.yml"
}

var regexpFromCompose = regexp.MustCompile(`(?m:^(\s*)image\s?:\s?"?([^"|\n]+)"?$)`)

type dockerComposeUpdater struct {
	config               Config
	parseDockerPkg       func(string) (dockerPkg, error)
	isExcludes           func(name string, excludes []string) bool
	fetchLatestDockerTag func(dockerPkg) (string, error)
}

func newDockerCompose(config Config) Updater {
	dockerUpdater := newDocker(config).(*dockerUpdater)

	u := &dockerComposeUpdater{
		config:               config,
		parseDockerPkg:       dockerUpdater.parseDockerPkg,
		isExcludes:           dockerUpdater.isExcludes,
		fetchLatestDockerTag: dockerUpdater.fetchLatest,
	}
	return u
}

func (u *dockerComposeUpdater) Update(_ string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	data, err := ioutil.ReadFile(pkg.Path)
	if err != nil {
		loggerDockerCompose.WithField("file", pkg.Path).WithError(err).Error("Can't read file")
		return nil, err
	}
	newDepth, dataStr := u.updateServices(data, excludes)
	if len(newDepth) > 0 {
		err = ioutil.WriteFile(pkg.Path, []byte(dataStr), 0666)
		if err != nil {
			loggerDockerCompose.WithError(err).Error("Can't fetch version")
		}
	}
	return newDepth, nil
}

func (u *dockerComposeUpdater) updateServices(data []byte, excludes []string) ([]project.Dependency, string) {
	newDepth := make([]project.Dependency, 0)
	lines := bytes.Split(data, []byte("\n"))
	var dataStr string
	for _, line := range lines {
		lineStr := string(line)
		results := regexpFromCompose.FindAllSubmatch(line, -1)
		if len(results) > 0 {
			result := results[0]
			space := string(result[1])
			name := string(result[2])
			dkPkg, err := u.parseDockerPkg(name)
			if err != nil {
				loggerDockerCompose.WithError(err).Error("Can't parse docker pkg")
				continue
			}
			displayName := dkPkg.displayName(u.config.Docker)
			if !u.isExcludes(displayName, excludes) {
				newVersion, err := u.fetchLatestDockerTag(dkPkg)
				if err != nil {
					loggerDockerCompose.WithError(err).Error("Can't fetch version")
				}
				if newVersion != "" {
					newDepth = append(newDepth, project.Dependency{Name: displayName, NewVersion: newVersion, OriginalVersion: dkPkg.oldVersion, CanBeExcludes: true})
					lineStr = string(dkPkg.toFile(newVersion, u.config.Docker, []byte(space+"image: "), []byte("")))
				}
			}
		}
		dataStr = dataStr + lineStr + "\n"
	}
	return newDepth, dataStr
}
