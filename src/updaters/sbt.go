// to know version :
// cat project/build.properties
// > sbt.version=0.13.16
// cat project/build.properties
// > sbt.version=1.1.1

// cd docker/updater/sbt/1.0/
// docker build -t sbt .
// cd /tmp
// git clone https://github.com/cheleb/cats-sandbox.git
// cd cats-sandbox
// docker run --rm -it -v $(pwd):/data -w="/data" sbt sbt "; dependencyUpdates"

// [info] Found 2 dependency updates for cats-sandbox
// [info]   org.spire-math:kind-projector:plugin->default(compile) : 0.9.3 -> 0.9.6
// [info]   org.typelevel:cats-core                                : 1.0.0 -> 1.0.1

// cd docker/updater/sbt/0.13
// docker build -t sbt0.13 .
// cd /tmp
// git clone https://github.com/cheleb/hello-dotty.git
// cd hello-dotty
// docker run -v $(pwd):/data -w="/data" sbt0.13 sbt "; dependencyUpdates"

// //NOT PROJECT BUT TRANSITIVE !!
// [info] Found 1 dependency update for dotty-simple
// [info]   ch.epfl.lamp:scala-library : 0.7.0-RC1 -> 0.7.0-bin-20180304-4cfffbe-NIGHTLY -> 0.8.0-bin-20180307-14fb071-NIGHTLY

package updaters

import (
	"bytes"
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"strings"

	"github.com/blang/semver"
	"github.com/sirupsen/logrus"
)

//SbtConfig is configuration required by sbt updater
type SbtConfig struct {
	Enabled   bool
	Image0_13 string
	Image1_0  string
	Command   []string
	Volumes   []string
	Env       []string
	DNS       []string
}

//DefaultSbtConfig is the default configuration required by pip updater
var DefaultSbtConfig = SbtConfig{
	Enabled:   true,
	Image0_13: "registry.gitlab.com/continuousevolution/continuous-evolution/sbt-0.13",
	Image1_0:  "registry.gitlab.com/continuousevolution/continuous-evolution/sbt-1.0",
	Command:   []string{"sbt", "; dependencyUpdates"},
	Volumes:   []string{},
	Env:       []string{},
	DNS:       []string{},
}

var (
	loggerSbt     = logrus.WithField("logger", "updaters/sbt")
	regexpProject = regexp.MustCompile(`(?m:^\[info\]\sFound\s*\d+\s*dependency\s*updates\s*for\s*(.*)$)`)
	regexpDep     = regexp.MustCompile(`(?m:^\[info\]\s*([^\s]+)\s*:\s*([^\s]+)\s*->\s*(.*)$)`)
)

func sbtIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "build.sbt"
}

type sbtUpdater struct {
	config       SbtConfig
	dockerClient tools.Docker
}

func newSbt(config Config) Updater {
	return &sbtUpdater{config: config.Sbt, dockerClient: tools.NewDocker()}
}

func (u *sbtUpdater) Update(projectPath string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	dirpath := path.Dir(pkg.Path)
	sbtVersionFile, err := ioutil.ReadFile(projectPath + "project/build.properties")
	if err != nil {
		return nil, err
	}
	image := u.config.Image1_0
	currentVersion := semver.MustParse(strings.Split(strings.TrimSpace(string(sbtVersionFile)), "=")[1])
	if currentVersion.Major == 0 && currentVersion.Minor == 13 {
		image = u.config.Image0_13
	}
	binds := append(u.config.Volumes, projectPath+":/tmp")
	logs := u.dockerClient.StartDocker(image, u.config.Command, binds, "/tmp", u.config.Env, u.config.DNS)
	outdatedDependencies, err := u.findOutdatedDependencies(dirpath, logs)
	if err != nil {
		return nil, err
	}
	outdatedDependenciesWithoutExcludes := u.removeExcludes(outdatedDependencies, excludes)
	return outdatedDependenciesWithoutExcludes, u.updateFile(projectPath, outdatedDependenciesWithoutExcludes)
}

func (u *sbtUpdater) findOutdatedDependencies(projectName string, logs []byte) ([]project.Dependency, error) {
	outdatedDep := make([]project.Dependency, 0)
	lines := bytes.Split(logs, []byte("\n"))
	isProject := false
	for _, line := range lines {
		lineProject := regexpProject.FindSubmatch(line)
		if len(lineProject) == 2 {
			isProject = string(lineProject[1]) == projectName
		}
		result := regexpDep.FindSubmatch(line)
		if isProject && len(result) == 4 {
			outdatedDep = append(outdatedDep, project.Dependency{Name: string(result[1]), OriginalVersion: string(result[2]), NewVersion: string(result[3]), CanBeExcludes: false})
		}
	}
	return outdatedDep, nil
}

func (u *sbtUpdater) removeExcludes(outdatedDependencies []project.Dependency, excludes []string) []project.Dependency {
	outdatedDependenciesWithoutExcludes := make([]project.Dependency, 0)
	for _, dep := range outdatedDependencies {
		found := false
		for _, exclude := range excludes {
			if dep.Name == exclude {
				found = true
				break
			}
		}
		if !found {
			outdatedDependenciesWithoutExcludes = append(outdatedDependenciesWithoutExcludes, dep)
		}
	}
	return outdatedDependenciesWithoutExcludes
}

func (u *sbtUpdater) updateFile(projectPath string, dependencies []project.Dependency) error {
	raw, err := ioutil.ReadFile(projectPath + "build.sbt")
	if err != nil {
		return err
	}
	lines := bytes.Split(raw, []byte("\n"))
	newFile := make([]string, 0)
	for _, l := range lines {
		line := string(l)
		newLine := ""
		for _, dep := range dependencies {
			group := strings.Split(dep.Name, ":")[0]
			name := strings.Split(dep.Name, ":")[1]
			version := dep.OriginalVersion
			if strings.Contains(line, group) && strings.Contains(line, name) && strings.Contains(line, version) {
				newLine = strings.Replace(line, version, dep.NewVersion, 1)
				break
			} else {
				newLine = line
			}
		}
		newFile = append(newFile, newLine)
	}
	return ioutil.WriteFile(projectPath+"build.sbt", []byte(strings.Join(newFile, "\n")), os.ModePerm)
}
