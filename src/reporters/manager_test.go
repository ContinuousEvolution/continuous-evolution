package reporters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"html/template"
	"testing"
)

func TestNewManager(t *testing.T) {
	m, err := NewManager(DefaultConfig)
	if err != nil {
		t.Fatal("DefaultConfig should not throw err", err)
	}
	if len(m.(*manager).reporters) != 3 {
		t.Fatal("Default config should enable all reporters")
	}
}

func TestNewManagerDisable(t *testing.T) {
	m, err := NewManager(Config{Git: DefaultGitConfig, Github: GithubConfig{Enabled: false}, Gitlab: GitlabConfig{Enabled: false}})
	if err != nil {
		t.Fatal("Good config should not throw err", err)
	}
	if len(m.(*manager).reporters) != 1 {
		t.Fatal("Manager should enable only git reporter")
	}
}

func TestReport(t *testing.T) {
	m, _ := NewManager(DefaultConfig)
	fakeNotAcceptReporter := mocks.NewFakeReporterDefaultFatal(t)
	fakeNotAcceptReporter.AcceptHook = func(project.Project) bool {
		return false
	}
	fakeAcceptReporter := mocks.NewFakeReporterDefaultFatal(t)
	fakeAcceptReporter.AcceptHook = func(project.Project) bool {
		return true
	}
	fakeAcceptReporter.ReportHook = func(project.Project, *template.Template) (project.Project, error) {
		return nil, nil
	}
	m.(*manager).reporters = []Reporter{fakeNotAcceptReporter, fakeAcceptReporter}
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	m.Report(project.NewOptional(fakeProject))
}

func TestReportWithoutReporter(t *testing.T) {
	m, _ := NewManager(DefaultConfig)
	m.(*manager).reporters = []Reporter{}
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.FullnameHook = func() string {
		return "fullname"
	}
	p := m.Report(project.NewOptional(fakeProject))
	if !p.IsError() {
		t.Fatal("No reporter should return an error")
	}
	if p.Get() == nil {
		t.Fatal("No reporter should return a valid project")
	}
}

func TestReportWithoutAcceptReporter(t *testing.T) {
	m, _ := NewManager(DefaultConfig)
	fakeNotAcceptReporter := mocks.NewFakeReporterDefaultFatal(t)
	fakeNotAcceptReporter.AcceptHook = func(project.Project) bool {
		return false
	}
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.FullnameHook = func() string {
		return "fullname"
	}
	m.(*manager).reporters = []Reporter{fakeNotAcceptReporter}
	p := m.Report(project.NewOptional(fakeProject))
	if !p.IsError() {
		t.Fatal("No accept reporter should return an error")
	}
	if p.Get() == nil {
		t.Fatal("No accept reporter should return a valid project")
	}
}

func TestReportMultiAccept(t *testing.T) {
	m, _ := NewManager(DefaultConfig)
	fakeAcceptReporter := mocks.NewFakeReporterDefaultFatal(t)
	fakeAcceptReporter.AcceptHook = func(project.Project) bool {
		return true
	}
	fakeAcceptReporter.ReportHook = func(project.Project, *template.Template) (project.Project, error) {
		return nil, nil
	}
	fakeAccept2Reporter := mocks.NewFakeReporterDefaultFatal(t)
	fakeAccept2Reporter.AcceptHook = func(project.Project) bool {
		return true
	}
	m.(*manager).reporters = []Reporter{fakeAcceptReporter, fakeAccept2Reporter}
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	m.Report(project.NewOptional(fakeProject))
	//this test fail if fakeAccept2Reporter.Report is called because only one reporter should be called
}
