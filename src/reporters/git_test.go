package reporters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"errors"
	"testing"
)

func TestGitAccept(t *testing.T) {
	g := newGit()
	fakeGit := &mocks.FakeGit{
		DiffHook: func() bool {
			return true
		},
	}
	fakeProject := &mocks.FakeProject{
		GitHook: func() (ident1 project.Git) {
			return fakeGit
		},
	}
	if !g.Accept(fakeProject) {
		t.Fatal("git should accept if diff")
	}
}

func TestGitNoAccept(t *testing.T) {
	g := newGit()
	fakeGit := &mocks.FakeGit{
		DiffHook: func() bool {
			return false
		},
	}
	fakeProject := &mocks.FakeProject{
		GitHook: func() (ident1 project.Git) {
			return fakeGit
		},
	}
	if g.Accept(fakeProject) {
		t.Fatal("git should not accept if no diff")
	}
}

type gitReportData struct {
	name        string
	errorCommit error
	errorPush   error
	valid       bool
}

var gitReportDatas = []gitReportData{
	{name: "valid", errorCommit: nil, errorPush: nil, valid: true},
	{name: "error commit", errorCommit: errors.New("Fake Commit error"), errorPush: nil, valid: false},
	{name: "error push force", errorCommit: nil, errorPush: errors.New("Fake PushForce error"), valid: false},
}

func TestGitReport(t *testing.T) {
	g := newGit()
	for _, data := range gitReportDatas {
		t.Run(data.name, func(t *testing.T) {
			fakeGit := &mocks.FakeGit{
				CommitHook: func() error {
					return data.errorCommit
				},
				PushForceHook: func() error {
					return data.errorPush
				},
			}
			fakeProject := &mocks.FakeProject{
				GitHook: func() (ident1 project.Git) {
					return fakeGit
				},
			}
			_, err := g.Report(fakeProject, nil)
			if data.valid && err != nil {
				t.Fatal("Valid test should not throw error", err)
			} else if !data.valid && err == nil {
				t.Fatal("Git error should throw error")
			}
		})
	}
}
