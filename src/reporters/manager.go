package reporters

//go:generate charlatan -package mocks -output ../mocks/managerReporter.go Reporter

import (
	"continuous-evolution/src/project"
	"errors"
	"html/template"

	"github.com/sirupsen/logrus"
)

//Config is configuration required by reporters
type Config struct {
	Enabled        bool
	PoolSize       int
	Advertizedhost string
	Git            GitConfig
	Github         GithubConfig
	Gitlab         GitlabConfig
}

//DefaultConfig is the default configuration required by reporters
var DefaultConfig = Config{Enabled: true, PoolSize: 1, Advertizedhost: "http://127.0.0.1:1234", Git: DefaultGitConfig, Github: DefaultGithubConfig, Gitlab: DefaultGitlabConfig}

var loggerManager = logrus.WithField("logger", "reporters/manager")

//Reporter is responsible to report updated depth
type Reporter interface {
	Accept(project.Project) bool
	Report(project.Project, *template.Template) (project.Project, error)
}

type manager struct {
	reporters           []Reporter
	tplMergeRequestBody *template.Template
}

//Manager is reposible to download the code
type Manager interface {
	Report(project.Option) project.Option
}

//NewManager return a Manager configured
func NewManager(config Config) (Manager, error) {
	reporters := make([]Reporter, 0)
	if config.Github.Enabled {
		reporters = append(reporters, newGithub(config))
	}
	if config.Gitlab.Enabled {
		reporters = append(reporters, newGitlab(config))
	}
	//git is last because previous will use git as subReporter and clean working directory so this git will don't accept
	if config.Git.Enabled {
		reporters = append(reporters, newGit())
	}

	tplMergeRequestBody := template.Must(template.New("BodyRequest").Parse(project.MergeRequestBody))

	return &manager{
		reporters:           reporters,
		tplMergeRequestBody: tplMergeRequestBody,
	}, nil
}

//Report will report updated depth depending on git url
func (m *manager) Report(o project.Option) project.Option {
	return o.ExecIfNoError(func(p project.Project) (project.Project, error) {
		for _, reporter := range m.reporters {
			if reporter.Accept(p) {
				return reporter.Report(p, m.tplMergeRequestBody)
			}
		}
		return p, errors.New("No updater found for " + p.Fullname())
	})
}
