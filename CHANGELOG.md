# Changelog

## Version 1.0.0 - [UNRELEASED]

[Milestone 1.0.0](https://gitlab.com/ContinuousEvolution/continuous-evolution/milestones/4)

## Version 0.11.0 - [UNRELEASED]

* Repair video in landing page [#142](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/142).
* Fix [goreportcard.com](https://goreportcard.com/report/gitlab.com/ContinuousEvolution/continuous-evolution) issues [#136](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/136).
* Add metrics and error [#128](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/128) and don't relaunch project in errors.
* Repair cover in CI [#147](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/147)
* Add junit report for gitlab-ci [#146](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/146).

## Version 0.10.0 - 2018/08/18

* Add version in configuration file, for now only "1.0.0" but it will permit to update configuration file more easily in future release [#140](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/140). By the way, add a deep merge between user conf and default conf, like that not fullfill conf will run as the default conf.
* Add a flag `cli -generate-config ./configuration.toml` to be able to generate default conf.
* Update :
  * golang from 1.10.1 to 1.10.3
  * [dep](https://github.com/golang/dep) from 0.4.1 to 0.5.0
  * [hugo](https://github.com/gohugoio/hugo) from v0.34 to v0.46
  * go dependencies
  * updater golangDep from golang:1.9.1-alpine36 to golang:1.10.3-alpine37 and dep 0.4.1 to 0.5.0
* Complete rework of git usage, now we *git clone --depth 1* and always do a new branch. When report we *git push -f* so the branch is always updated with latest code. Counterpart is we lose history of the branch [#97](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/97)
* Better CI for charlatan (mocks) [#134](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/134)
* **project/io.HTTP** now serialize and deserialize json [#130](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/130)
* Add project by static member instead of token [#126](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/126). Add two orchestrators (github and gitlab), configure them with :

```toml
[orchestrator.github]
    enabled=false
    durationfetch="1h"
    [[orchestrator.github.instances]]
        apiurl="https://api.github.com"
        login=""
        privatetoken=""
[orchestrator.gitlab]
    enabled=false
    durationfetch="1h"
    [[orchestrator.github.instances]]
        apiurl="https://gitlab.com/api"
        login=""
        privatetoken=""
```

* Describe new static member import in landing page with videos [#131](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/131)
* Permit to relaunch process without login/token in url (usefull for static user but also for standard workflow) [#129](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/129)
* Downloaders need host of distant Github, Gitlab. Permit to add Github on premise [#128](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/128) and secure Gitlab without call it [#27](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/27). This is the new configuration (default) :

```toml
[downloader]
poolsize=1
pathtowrite = "/tmp"
branchname = "ConEvolUpdateLibs"
    [downloader.local]
    enabled=true
    [downloader.git]
    enabled=true
    gitlaburl=["gitlab.com"]
    githuburl=["github.com"]
    [downloader.pullrequest]
    enabled=true
    url=["github.com"]
    [downloader.mergerequest]
    enabled=true
    url=["gitlab.com"]
```

## Version 0.9.0 - 2018/04/28

* Add sbt updater [#100](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/100)
* Repair CI in fork [#116](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/116)
* Update [hugorha theme](https://github.com/itkSource/hugorha) and add develop page with cards of subpage [#120](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/120)
* Fix some typo in landing page [#119](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/119)
* Fix table landing page [#118](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/118)
* Transform project.Project from struct to interface to not update struct and to add some checks. BTW more tests are possible [#90](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/90)
* Build [npm-check-update](https://github.com/tjunnone/npm-check-updates) image to be up to date [#127](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/127)
* Increase test coverage to ~70% [!108](https://gitlab.com/ContinuousEvolution/continuous-evolution/merge_requests/108)
* When project.io.http receive bad http status code (e.g <200 or >=300) return an error to break chain [#107](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/107) and [#123](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/123)

## Version 0.8.1 - 2018/03/07

* Bad url in gitlab reporter for scheduler [#114](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/114)

## Version 0.8.0 - 2018/03/07

* Docker updater can update more than one FROM (multi layer) [#69](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/69)
* Save only required properties of a project in scheduler [#112](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/112)
* Set good url (login:token@host...) in project at the end of reporter, to be re-used by scehduler [#113](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/113)
* Rename package **model** into **project** [#103](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/103)
* Update to golang 1.10 and update dependencies [#111](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/111)

## Version 0.7.0 - 2018/02/24

* Add model.Project.IsSame function to compare two projects, and use it in scheduler [#109](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/109)
* Secure deleter [#110](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/110) by :
  * Check if PathToWrite and Name project are no empty
  * Remove creation of empty project in all chains
* When user close (merge | pull)-request continuous-evolution must delete project [#104](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/104)
* Update project.GitURL in gitlab/github reporter to reprocess on next scheduler [#108](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/108)
* Add a warning about **git push -force** in landing page and change default branch from **UpdateLIbs** to **ConEvolUpdateLibs** [#106](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/106)

## Version 0.6.0 - 2018/02/17

* fix bug : a merged/closed (merge | pull)-request was not reopen on next process [#83](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/83)
* fix bug : scheduler don't relaunch job after 0.5.1 rework [#101](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/101)
* better landing page and add updater doc [#64](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/64)
* repair golint in CI [#99](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/99)
* delete project at end also when project crash [#96](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/96)

## Version 0.5.1 - 2018/02/10

* fix bug : git clone is called 2 times [#98](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/98)

## Version 0.5.0 - 2018/02/10

* docker compose updater now support excludes [#85](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/85)
* golangDep updater now support excludes [#94](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/94)
* Fix bug : excluded packages was not reported in next update [#93](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/93)
* Delete directory of cloned code at the end of process (instead of first of next call) [#42](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/42) and make this deletion with docker, to avoid failure when file is created by docker [#78](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/78)
* Speed CI pipeline by sending vendor directory between jobs with artifacts [#66](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/66)
* Rework internal processing chain to be able to set nb worker by task [#72](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/72), delete distributed mode for now.
* Update golangDep to 0.4.1 [#82](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/82)
* Static site improvments [#87](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/87) :
  * add margin after font icone
  * set 2018 year in footer
  * adapte responsive header
* fix bug [#91](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/91) dependencies of docker updater can be excluded
* Externalize configuration [#84](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/84) this generate a **breaking change** and you need to delete previous scheduler file :
  * pathtowrite : tmp dir to git clone code when web is active
  * branchname : git branch name continuous will use to update dependencies

## Version 0.4.0 - 2018/01/28

* docker generated by continuous-evolution are now auto-remove [#88](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/88)
* add excludes for docker updater [#86](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/86) and fix docker and docker-compose report name of updated dependencies
* don't add checkbox in report when dependency can't be excluded [#63](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/63)
* permit to set the delay between 2 process of a project [#69](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/69)
* set advertized host (public access to continuous-evolution) in configuration of reporters [#79](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/79)
* report excludes in (merge | pull)-request body [#80](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/80)
* update (merge | pull)-request body on reprocess to avoid false version number between 2 rounds [#77](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/77)
* fix bug [#76](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/76) when an old (merge | pull)-request was reprocessed, the diff code between branch and master was added.
* golang/dep updater report versions of updated dependencies [#44](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/44)

## Version 0.3.0 - 2018/01/13

* add steps in gitlab-ci.yml [#73](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/73) before a merge-request was validated the latest artifact binary and latest docker was updated. Now only build and run are validated but nothing is pushed.
* docker-compose updater [#68](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/68) fix bug : when use multiple same service, continuous-evolution updated only first
* add entrypoint in Dockerfile [#60](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/60) before you do **docker run ... registry.gitlab.com/continuousevolution/continuous-evolution cli -version** after **docker run ... registry.gitlab.com/continuousevolution/continuous-evolution -version**
* scheduler bug [#71](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/71)

## Version 0.2.0 - 2018/01/06

* gradle updater [#14](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/14) with exclude [#52](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/52) and report [#51](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/51)
* config file [#48](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/48) permit to config continuous-evolution with a **configuration.toml**, a full example can be found in [master/configurtion.toml](https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/configuration.toml)
* licence changed to AGPL [#59](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/59)
* rework static site [#53](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/53)
* edit with Merge-Request [#43](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/43) :
  * add **downloader/mergeRequest** downloader
  * update **reporter/gitlab** to add link to relanch process after user as unselect dependencies

## Version 0.1.0 - 2017/11/28

* Main workflow ok :
  * downloader : git, local, pull-request
  * updater : docker, docker-compose, golangDep, maven, npm, pip
  * reporter : git, github, gitlab
* Scheduler ok : use `cli -scheduler-config-path /absolute/path` to activate it
* Start for web (port 1234) use `cli -web` or for cli use `cli -url /absolute/path`
* Distributed mode in progress
* Doc in progress [site](https://continuousevolution.gitlab.io/continuous-evolution/)