---
title: "FAQ"
menu: 
    main:
        weight: 4
        pre: '<i class="fa fa-question-circle" aria-hidden="true"></i>'
subnav: "true"
description: "No question are stupid, only response can be."
---
# FAQ

## WTF is (merge | pull)-request ?

Continuous Evolution don't want to chose between gitlab (merge-request) and github (pull-request). So we use this terme. An other term could be **git-request**, if you prefer it open an [issue](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues) ;)

## Why do you need private token ?

To make a (merge | pull)-request we need to have access to your account. We could use OAuth2 (both gitlab and github provide it) in future for for now we prefer to have more package manager and stabilize platforme before implementing this kind of feature. Anyway, Continuous Evolution ❤ [merge-request](https://gitlab.com/ContinuousEvolution/continuous-evolution/merge_requests) ;)

## How to obtain private token on gitlab ?

Follow [gitlab instruction](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html). Only **api** scope is requiered.

## How to obtain private token on github ?

Follow [github instruction](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/). Only **repo** scope is requiered.

## I don't want to give you access to my main repo

No problemo! Fork your main project and give access to the fork. When a (merge | pull)-request is done redirect it to the main project. A good feature for ContinuousEvolution could be to have an option to send (merge | pull)-request to main repository. If you want it open an [issue](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues) ;)

## What happend if I don't accept (merge | pull)-request ?

Nothing, next time continuous-evolution will process your project, it will reuse same branch and update it.

## Why every week ?

Week time is a good timelapse, not often, not too much... Anyway if you want every day or every month open an [issue](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues) ;) And lets talk about that!

## I want to pay you

You don't need to do this. We have regular job to eat and we want to give to the open community this tool.

## I really want to pay you

Contact us (issue, email or twitter...) we ❤ beer :D